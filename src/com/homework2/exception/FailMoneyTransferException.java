package com.homework2.exception;

public class FailMoneyTransferException extends RuntimeException {

    FailMoneyTransferException() {
    }

    public FailMoneyTransferException(String msg) {
        super(msg);
    }
}
