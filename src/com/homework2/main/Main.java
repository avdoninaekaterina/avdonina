package com.homework2.main;

import com.homework2.entities.*;
import com.homework2.enums.Currency;

public class Main {

    public static boolean checkEqualsMethod() {
        boolean isWork;
        Person user1 = new Person("Петр", "Петров", "Петрович", "5959 6060 3333 8888", 1234, 1024.08);
        Person user2 = new Person("Петр", "Петров", "Петрович", "5959 6060 3333 8888", 1234, 1024.08);
        Person user3 = new Person("Петр", "Петров", "Петрович", "5959 6060 3333 8888", 1234, 1024.08);

        isWork = user1.equals(user1);
        System.out.println("Рефлексивность: " + isWork);
        isWork = user1.equals(user2) && user2.equals(user1);
        System.out.println("Симметричность: " + isWork);
        isWork = user1.equals(user2) && user2.equals(user3) && user3.equals(user1);
        System.out.println("Транзитивность:" + isWork);
        for (int i = 0; i < 100; i++) {
            if (i == 89) {
                user3.setFirstName("Иван");
            }
            if (!user2.equals(user3)) {
                isWork = !user2.equals(user3);
                System.out.println("Согласованность:" + isWork);
                break;
            }
        }
        isWork = !user3.equals(null);
        System.out.println("Сравнение null: " + isWork);
        return isWork;
    }

    public static void checkHashCodeMethod() {
        Person user1 = new Person("Петр", "Петров", "Петрович", "5959 6060 3333 8888", 1234, 1024.08);
        System.out.println("Петр Петров: " + user1.hashCode());
        Person user2 = new Person("Петр", "Петров", "Петрович", "5959 6060 3333 8888", 1234, 1024.08);
        System.out.println("Петр Петров: " + user2.hashCode());
        Person user3 = new Person("Иван", "Иванов", "Иванович", "5959 6060 3333 8888", 1234, 1024.08);
        System.out.println("Иван Иванов: " + user3.hashCode());
    }

    public static void main(String[] args) {
        Recipient recipient = new Recipient("Петр", "Петров", "Петрович", "5959 6060 3333 8888", 1234, 1024.08);
        Sender sender = new Sender("Иван", "Иванов", null, "7474 8585 9696 4141", 4321, 7490.05);

        TransferCardToCard cardToCard = new TransferCardToCard( 100.0, Currency.USD, recipient, sender);
        cardToCard.doTransfer(); //successful

        cardToCard.setAmountOfMoney(9562.0);
        cardToCard.doTransfer(); //unsuccessful

        TransferAccountToAccount accountToAccount = new TransferAccountToAccount( 100.0, Currency.USD, recipient, sender);
        accountToAccount.doTransfer(); //successful

        accountToAccount.setAmountOfMoney(9562.0);
        accountToAccount.doTransfer(); //unsuccessful

        recipient.getFullNameToUpperCase();
        recipient.getFullNameToLowerCase();

        sender.getFullNameToUpperCase();
        sender.getFullNameToLowerCase();

        System.out.println("Проверка работы метода equals: " + checkEqualsMethod());
        System.out.println("Проверка работы метода hashCode: ");
        checkHashCodeMethod();
    }

}
