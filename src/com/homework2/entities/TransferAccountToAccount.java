package com.homework2.entities;

import com.homework2.enums.Currency;

public class TransferAccountToAccount extends Transfer {

    public TransferAccountToAccount(Double amountOfMoney, Currency currency, Recipient recipient, Sender sender) {
        super(amountOfMoney, currency, recipient, sender);
    }

    @Override
    public String getTransferInfo() {
        return "ПЕРЕВОД СО СЧЕТА НА СЧЕТ: " +
                "Номер перевода: " + numberTransfer + ", " +
                "Дата: " + dateFormat.format(dateTransfer.getTime()) + ", " +
                "Номер счета получателя: " + recipient.getAccountNumber() + ", " +
                "Номер счета отправителя: " + sender.getAccountNumber() + ", " +
                "Валюта: " + currency.name() + ", " +
                "Сумма: " + amountOfMoney + ", " +
                "Получатель: " + recipient.getFullName() + ", " +
                "Отправитель: " + sender.getFullName() + ".";
    }
}
