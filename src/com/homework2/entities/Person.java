package com.homework2.entities;

import java.util.Locale;
import java.util.Objects;

public class Person {

    protected String firstName;
    protected String lastName;
    protected String patronymic;
    protected String cardNumber;
    protected Integer accountNumber;
    protected Double amountOfMoney;

    public Person(String firstName, String lastName, String patronymic, String cardNumber, Integer accountNumber, Double amountOfMoney) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.cardNumber = cardNumber;
        this.accountNumber = accountNumber;
        this.amountOfMoney = amountOfMoney;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Integer getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(Integer accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Double getAmountOfMoney() {
        return amountOfMoney;
    }

    public void setAmountOfMoney(Double amountOfMoney) {
        this.amountOfMoney = amountOfMoney;
    }

    public String getFullName() {
        if (patronymic != null) {
            return lastName + " " + firstName + " " + patronymic;
        } else {
            return lastName + " " + firstName;
        }
    }

    public String getFullNameToUpperCase() {
        return getFullName().toUpperCase(Locale.ROOT);
    }

    public String getFullNameToLowerCase() {
        return getFullName().toLowerCase(Locale.ROOT);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o == null || o.getClass() != this.getClass()) {
            return false;
        }

        Person person = (Person) o;
        return Objects.equals(firstName, person.firstName)
                && Objects.equals(lastName, person.lastName)
                && Objects.equals(patronymic, person.patronymic);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
        result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
        result = prime * result + ((patronymic == null) ? 0 : patronymic.hashCode());
        return result;
    }
}
