package com.homework2.entities;

import com.homework2.enums.Currency;

public class TransferCardToCard extends Transfer {

    public TransferCardToCard(Double amountOfMoney, Currency currency, Recipient recipient, Sender sender) {
        super(amountOfMoney, currency, recipient, sender);
    }

    @Override
    public String getTransferInfo() {
        return "ПЕРЕВОД С КАРТЫ НА КАРТУ: " +
                "Номер перевода: " + numberTransfer + ", " +
                "Дата: " + dateFormat.format(dateTransfer.getTime()) + ", " +
                "Номер карты получателя: " + recipient.getCardNumber() + ", " +
                "Номер карты отправителя: " + sender.getCardNumber() + ", " +
                "Валюта: " + currency.name() + ", " +
                "Сумма: " + amountOfMoney + ", " +
                "Получатель: " + recipient.getFullName() + ", " +
                "Отправитель: " + sender.getFullName() + ".";
    }
}
