package com.homework2.entities;

public class Sender extends Person {

    public Sender(String firstName, String lastName, String patronymic, String cardNumber, Integer accountNumber, Double amountOfMoney) {
        super(firstName, lastName, patronymic, cardNumber, accountNumber, amountOfMoney);
    }

    public boolean checkAmountOfMoney(Double amountOfMoney){
        return this.amountOfMoney >= amountOfMoney;
    }
}
