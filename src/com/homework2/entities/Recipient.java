package com.homework2.entities;

public class Recipient extends Person {

    public Recipient(String firstName, String lastName, String patronymic, String cardNumber, Integer accountNumber, Double amountOfMoney) {
        super(firstName, lastName, patronymic, cardNumber, accountNumber, amountOfMoney);
    }
}
