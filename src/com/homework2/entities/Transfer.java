package com.homework2.entities;

import com.homework2.enums.Currency;
import com.homework2.exception.FailMoneyTransferException;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public abstract class Transfer {

    protected Integer numberTransfer;
    private static int transferCount = 0;
    protected Double amountOfMoney;
    protected Currency currency;
    protected Recipient recipient;
    protected Sender sender;
    protected Calendar dateTransfer;
    protected static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

    public Transfer(Double amountOfMoney, Currency currency, Recipient recipient, Sender sender) {
        this.amountOfMoney = amountOfMoney;
        this.currency = currency;
        this.recipient = recipient;
        this.sender = sender;
    }

    public Integer getNumberTransfer() {
        return numberTransfer;
    }

    public void setNumberTransfer(Integer numberTransfer) {
        this.numberTransfer = numberTransfer;
    }

    public Double getAmountOfMoney() {
        return amountOfMoney;
    }

    public void setAmountOfMoney(Double amountOfMoney) {
        this.amountOfMoney = amountOfMoney;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Recipient getRecipient() {
        return recipient;
    }

    public void setRecipient(Recipient recipient) {
        this.recipient = recipient;
    }

    public Sender getSender() {
        return sender;
    }

    public void setSender(Sender sender) {
        this.sender = sender;
    }

    public Calendar getDateTransfer() {
        return dateTransfer;
    }

    public void setDateTransfer(Calendar dateTransfer) {
        this.dateTransfer = dateTransfer;
    }

    public void doTransfer() {
        try {
            numberTransfer = ++transferCount;
            dateTransfer = Calendar.getInstance();
            if (sender.checkAmountOfMoney(amountOfMoney)) {
                recipient.setAmountOfMoney(sender.getAmountOfMoney() + amountOfMoney);
                sender.setAmountOfMoney(recipient.getAmountOfMoney() - amountOfMoney);
                System.out.println(getTransferInfo());
            } else {
                throw new FailMoneyTransferException("Перевод совершить нельзя, недостаточно средств");
            }
        } catch (FailMoneyTransferException e) {
            System.out.println(e.getLocalizedMessage());
        }
    }

    protected abstract String getTransferInfo();
}
